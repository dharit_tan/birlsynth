// opens MIDI input devices one by one, starting from 0,
// until it reaches one it can't open.  then waits for
// midi events on all open devices and prints out the
// device, and contents of the MIDI message

// devices to open (try: chuck --probe)
Hid hid;

// number of devices
0 => int device;

    // open the device
if( !hid.openKeyboard( device ) )
{
        me.exit();
}
spork ~ go( hid );

<<< "opened keyboard: SUCCESS" >>>;

// Steps to initialize Birl:
// 1. Set desired tuning system.
// 2. If using custom, populate custom tuning.
// 3. Set a fundamental.

// 0: equal tempered
// 1: just intonation
// 2: meantone
// 3: highland bagpipe
// 4: custom (must call setTuningCustom() with set of 11 freqs first)

BirlPhysicalModel t => Gain g1 => dac;

// harmonic minor scale
// (622.25, 587.33, 523.25, 493.88, 415.30, 392.00, 349.23, 311.13, 293.66, 261.63, 246.94) => t.setCustomTuning;

0 => t.tuning;
440 => float Fc;
Fc => t.setFundamental;

// Easier to start with all closed on computer keyboard.
// 0 = closed, 1 = open.
(0, 0.0) => t.toneHole;
(1, 0.0) => t.toneHole;
(2, 0.0) => t.toneHole;
(3, 0.0) => t.toneHole;
(4, 0.0) => t.toneHole;
(5, 0.0) => t.toneHole;
(6, 0.0) => t.toneHole;
(7, 0.0) => t.toneHole;
(8, 0.0) => t.toneHole;
(9, 0.0) => t.toneHole;


// infinite time loop
while( true ) 1::second => now;

// handler for one midi event
fun void go( Hid min )
{
    // the message
    HidMsg msg;
    
    // infinite event loop
    while( true )
    {
        // wait on event
        min => now;
        
        // print message
        while( min.recv( msg ) )
        {    
            if (msg.isButtonDown()) {
                // breathPressure
                if (msg.ascii == 65) {
                    0.6 => t.breathPressure;
                }
                if (msg.ascii == 83) {
                    (0, 1.0) => t.toneHole;
                }
                if (msg.ascii == 68) {
                    (1, 1.0) => t.toneHole;
                }
                if (msg.ascii == 70) {
                    (2, 1.0) => t.toneHole;
                }
                if (msg.ascii == 71) {
                    (3, 1.0) => t.toneHole;
                }
                if (msg.ascii == 72) {
                    (4, 1.0) => t.toneHole;
                }
                if (msg.ascii == 74) {
                    (5, 1.0) => t.toneHole;
                }
                if (msg.ascii == 75) {
                    (6, 1.0) => t.toneHole;
                }
                if (msg.ascii == 76) {
                    (7, 1.0) => t.toneHole;
                }
                if (msg.ascii == 59) {
                    (8, 1.0) => t.toneHole;
                }
            }
            
            else {
                // breathPressure
                if (msg.ascii == 65) {
                    0.0 => t.breathPressure;
                }
                if (msg.ascii == 83) {
                    (0, 0.0) => t.toneHole;
                }
                if (msg.ascii == 68) {
                    (1, 0.0) => t.toneHole;
                }
                if (msg.ascii == 70) {
                    (2, 0.0) => t.toneHole;
                }
                if (msg.ascii == 71) {
                    (3, 0.0) => t.toneHole;
                }
                if (msg.ascii == 72) {
                    (4, 0.0) => t.toneHole;
                }
                if (msg.ascii == 74) {
                    (5, 0.0) => t.toneHole;
                }
                if (msg.ascii == 75) {
                    (6, 0.0) => t.toneHole;
                }
                if (msg.ascii == 76) {
                    (7, 0.0) => t.toneHole;
                }
                if (msg.ascii == 59) {
                    (8, 0.0) => t.toneHole;
                 }
            }
            
            // print out midi message with id
            <<< msg.ascii >>>;
        }
    }
}
